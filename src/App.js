import ConfigurationPanel from './components/ConfigurationPanel';
import ConfigurationSchema from './components/ConfigurationSchema';
import "./App.css"

import styled from 'styled-components';
import { DndContext, useDndMonitor } from '@dnd-kit/core';
import { useEffect, useState } from 'react';
import Table from './components/Table';
const ConfigurationWrapper = styled.div`
  display: flex;
  flex-direction: row;
  align-items: flex-start;
`

const Monitor = ({ onDragSucceed }) => {
  useDndMonitor({
    // onDragStart(event) { console.log(event) },
    // onDragMove(event) { console.log(event) },
    // onDragOver(event) { console.log(event) },
    onDragEnd(event) { onDragSucceed(event) },
    // onDragCancel(event) { console.log(event) },
  });

  return <></>
}

function App() {
  const [panelItems, setItems] = useState([])
  const initialState = [{ id: 1, position: 1 },
  { id: 2, position: 2 },
  { id: 3, position: 3 },
  { id: 4, position: 4 },
  { id: 5, position: 5 },
  { id: 6, position: 6 },
  { id: 7, position: 7 },
  { id: 8, position: 8 }]

  const [slots, setSlots] = useState(initialState)

  useEffect(() => {
    setItems([
      { id: 1, name: "M1", port: "A1", receptacle: 'Busbar', plug: "M8" },
      { id: 2, name: "M2", port: "A2", receptacle: 'PL00X-301-10M8', plug: "PL18X-301-70 Orange" },
      { id: 3, name: "M3", port: "NEG", receptacle: 'Busbar', plug: "PL18Y-301-70 Black" },
      { id: 4, name: "M4", port: "B2", receptacle: 'PL082X-61-6', plug: "PL182X-61-6" },
      // { id: 5, name: "M5" },
    ])
  }, [])

  const onDragSucceed = ({ active, over, ...other }) => {
    console.log(active, over)
    if (!active || !over) return
    const itemId = active.data.current.id
    const slotId = over.data.current.slot.id
    const slot = slots.find(({ id }) => id === slotId)
    const item = panelItems.find(({ id }) => id === itemId)
    slot.data = { item }
    setSlots([...slots])
  }

  return (
    <div className="App">
      <h1 className="mb-4 text-4xl font-extrabold leading-none tracking-tight text-gray-900 md:text-4xl lg:text-5xl">
        BLV_131_01 -
        <span className="text-green-600"> DRAFT</span>
      </h1>
      <ConfigurationWrapper>
        <DndContext>
          <Monitor onDragSucceed={onDragSucceed} />
          <ConfigurationPanel items={panelItems} />
          <ConfigurationSchema slots={slots} />
        </DndContext>
        <Table slots={slots} />

      </ConfigurationWrapper>
      <button className="bg-blue-500 hover:bg-blue-700 text-white m-2 font-bold py-2 px-4 rounded">Save</button>
      <button className="bg-gray-500 hover:bg-gray-700 text-white font-bold py-2 px-4 rounded" onClick={() => setSlots(initialState)}>Reset</button>
    </div>
  );
}

export default App;
