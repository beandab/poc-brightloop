import React from "react";
import styled from "styled-components";
import { useDraggable } from '@dnd-kit/core';

const PanelWrapper = styled.div`
  padding: 0 10px;
`
const Item = ({ item: { id, name }, className, children }) => {
  const { attributes, listeners, setNodeRef, transform } = useDraggable({
    id: 'draggable' + id,
    data: {
      id
    }
  });

  let style = transform ? {
    transform: `translate3d(${transform.x}px, ${transform.y}px, 0)`,
  } : {};

  return (<div className={className} ref={setNodeRef} style={style} {...listeners} {...attributes}>
    {name}
  </div>)
}
const PanelItem = styled(Item)`
  height: 90px;
  width: 90px;
  background: white;
  margin: 5px;
  border-radius: 5px;
  background: #5081bd;
  border: 4px solid #3a5e89;
  display: flex;
   align-items: center;
  justify-content: center;
  font-size: 20px;
  font-weight: bolder;
  color: white;
`

const ConfigurationPanel = ({ items }) => {
  return <PanelWrapper>

    {items.map((item) => <PanelItem item={item} />)}

  </PanelWrapper>
}

export default ConfigurationPanel