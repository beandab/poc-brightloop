import React from "react";
import styled from "styled-components";
import { useDroppable } from '@dnd-kit/core';

const SlotItem = styled.div`
  flex-grow: 1;
    flex: 1;
  display: flex;
  justify-content: center;
  flex-direction: column;
`
const SlotBreak = styled.div`
  flex-basis: 100%;
  height: 0

`
const SlotWrapper = styled.div`

  width: 500px;
  display: flex;
  flex-wrap: wrap;
  box-sizing: border-box;
  padding: 10px 50px;
  border-radius: 10px;
  background: rgb(213 224 255);
  position: relative;
  z-index: -1;
`
const SlotContent = styled.div`
  height: 90px;

  min-width: 30px;
  background: white;
  margin: 5px;
  border-radius: 5px;
  background: #ff53006e;
  border: 4px solid #ff5300;
  display: flex;
   align-items: center;
  justify-content: center;
  font-size: 20px;
  font-weight: bolder;
  color: white;
`

const Rectangle = styled.div`
  width: 40px;
  height: 80px;
  position: absolute;
  background: white;
  bottom: 0;
  ${(props) => props.position === 'left' ?
    "left: 0; border-radius: 0px 50px 0 0;" :
    "right: 0; border-radius: 50px 0px 0 0;"
  }
`

const NormalSlot = ({ position }) => <SlotContent>S{position}</SlotContent>
const ConfigurationSlot = ({ slot }) => {
  const { data } = slot;
  const style = {
    background: "#5081bd",
    border: "4px solid #3a5e89",
  }
  return <SlotContent style={style}>
    {data.item.name}
  </SlotContent>
}

const Slot = ({ slot }) => {
  const { id, position, data } = slot
  const isBreakable = position % 4 === 0
  const { setNodeRef } = useDroppable({
    id: 'droppable' + id,
    data: {
      slot
    }
  });

  return <>
    <SlotItem ref={setNodeRef}>
      {data ? <ConfigurationSlot slot={slot} /> : <NormalSlot position={position} />}
    </SlotItem>
    {isBreakable ? <SlotBreak /> : null}
  </>
}

const ConfigurationSchema = ({ slots }) => {
  return <SlotWrapper className="">
    {slots.map((slot) => <Slot slot={slot} />)}
    <Rectangle position="left" />
    <Rectangle />
  </SlotWrapper>
}

export default ConfigurationSchema