import React from "react";


const Table = ({ slots }) => {

  return (
    < div className="relative overflow-x-auto p-2" >
      <table className="w-full text-sm text-left text-gray-500 dark:text-gray-400">
        <thead className="text-xs text-gray-700 uppercase bg-gray-50 dark:bg-gray-700 dark:text-gray-400">
          <tr>
            <th scope="col" className="px-6 py-3">
              Location
            </th>
            <th scope="col" className="px-6 py-3">
              Port
            </th>
            <th scope="col" className="px-6 py-3">
              Receptacle
            </th>
            <th scope="col" className="px-6 py-3">
              Plug
            </th>
          </tr>
        </thead>
        <tbody>
          {slots.filter((slot) => { return slot.data }).map((slot) => {
            const { port, receptacle, plug } = slot.data.item
            return <tr className="bg-white border-b dark:bg-gray-800 dark:border-gray-700">
              <th scope="row" className="px-6 py-4 font-medium text-gray-900 whitespace-nowrap dark:text-white">
                S{slot.position}
              </th>
              <td className="px-6 py-4">
                {port}
              </td>
              <td className="px-6 py-4">
                {receptacle}
              </td>
              <td className="px-6 py-4">
                {plug}
              </td>
            </tr>
          })}

        </tbody>
      </table>
    </div >)

}
export default Table;